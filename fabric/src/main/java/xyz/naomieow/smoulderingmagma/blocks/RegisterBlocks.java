package xyz.naomieow.smoulderingmagma.blocks;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import xyz.naomieow.smoulderingmagma.MagmaMod;

public class RegisterBlocks {
    public static final Block SMOULDERING_MAGMA = new SmoulderingMagmaBlock(FabricBlockSettings.copyOf(Blocks.MAGMA_BLOCK));
    private static BlockItem registerBlockItem(String id, Block block) {
        BlockItem item = Registry.register(
                Registries.ITEM,
                new Identifier(MagmaMod.MODID, id),
                new BlockItem(block, new FabricItemSettings())
        );
        return item;
    }

    private static BlockItem registerBlock(String id, Block block) {
        Registry.register(
                Registries.BLOCK,
                new Identifier(MagmaMod.MODID, id),
                SMOULDERING_MAGMA
        );
        return registerBlockItem(id, block);
    }
    public static void registerBlocks() {
        BlockItem smoulderingMagma = registerBlock("smouldering_magma", SMOULDERING_MAGMA);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.NATURAL).register(content -> {
            content.addAfter(Blocks.MAGMA_BLOCK, smoulderingMagma);
        });
    }
}

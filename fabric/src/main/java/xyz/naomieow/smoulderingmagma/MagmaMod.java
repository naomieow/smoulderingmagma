package xyz.naomieow.smoulderingmagma;

import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.naomieow.smoulderingmagma.blocks.RegisterBlocks;

public class MagmaMod implements ModInitializer {
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
    public static final Logger LOGGER = LoggerFactory.getLogger("smoulderingmagma");
	public static final String MODID = "smoulderingmagma";

	@Override
	public void onInitialize() {
		LOGGER.info("Registering Blocks..");
		RegisterBlocks.registerBlocks();
	}
}